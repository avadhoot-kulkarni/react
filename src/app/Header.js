import React from "react";

export class Header extends React.Component{
    render() {
        return(
            <nav className="navbar navbar-default">
                <div className="container">
                    <div className="navebar-header">
                        <ul className="nav navbar-nav">
                            <li>First item</li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

