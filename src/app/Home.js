import React from "react";
import PropTypes from 'prop-types';

export class Home extends React.Component {
    constructor(props) {
        super();
        this.state = {
            age: props.initialAge 
        };
        console.log('inside constructor',this.state.age);
    }
    onMakeOlder() {
        this.setState({
            age: this.state.age + 3
        });
        console.log('inside onMakeLoader function', this.state.age);
    }
    render() {
        
        return (
            <div>

                <p>Your name is {this.props.name}, your age is {this.state.age}.</p> 
                <button className="btn btn-primary" onClick={this.onMakeOlder.bind(this)}>Make me older</button>
                {console.log('after button',this.state.age)}
                <hr/>
                <p>Now the age is {this.state.age}</p>

            </div>
            
        );
    }
}

Home.propTypes = {
    name: PropTypes.string,
    initialAge: PropTypes.number,
    age: PropTypes.object
}